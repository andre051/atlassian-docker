General help for using docker compose scripts.

# Starting

    docker-compose -f <docker-compose-file.yml> up -d

Some scripts expect parameters to be passed via environment variables. Refer to a specific script file for details.

# Stopping

    docker-compose -f <docker-compose-file.yml> down