#!/bin/bash
echo "Installing FishEye and Crucible..."
yum update -y
yum install -y docker
service docker start
usermod -a -G docker ec2-user
docker pull mswinarski/atlassian-fisheye:latest
mkdir -p /home/ec2-user/atlassian/data/fisheye
docker run -d -p 80:8080 -v /home/ec2-user/atlassian/data/fisheye:/atlassian/data/fisheye --name fecru mswinarski/atlassian-fisheye:latest
echo "...done innstalling FishEye and Crucible"

